package j_tan.muchword;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Ziggy on 7/5/2016.
 */

/* DBUpdater Class
 * This class checks the S3 bucket for a new database version by comparing the remote database version file with the current version.
 * It alerts the user if a new version is available and initiates a database download if the user agrees to update.
 */

public class DBUpdater extends AsyncTask <URL, Integer, Long>  {
    public DBUpdater(Activity activity) {
        currentActivity = activity;
    }

     // The current activity is needed to successfully execute alert dialogs
    Activity currentActivity;
    Float remoteDBVersion;

    // A variable to track if a new database version is available
    Boolean dBUpdateAvailable = false;

    // An alert dialog to ask the user if they want to update the database if a new version is available
    AlertDialog.Builder builder;

    // Create a new alert dialog builder
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        builder = new AlertDialog.Builder(currentActivity);
    }

    // Main execution call creates a connection to the version file and check its contents
    /* References: HttpURLConnection on Android Developer - http://developer.android.com/reference/java/net/HttpURLConnection.html
     *             Connecting to the Network on Android Developer - http://developer.android.com/training/basics/network-ops/connecting.html
     */
    @Override
    protected Long doInBackground(URL... params) {
        try {
            URL url = new URL("https://s3-ap-southeast-2.amazonaws.com/muchword/version.txt");
            HttpURLConnection httpURLconnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = new BufferedInputStream(httpURLconnection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();
            String currentLine;
            while ((currentLine = bufferedReader.readLine()) != null) {
                stringBuilder.append(currentLine);
            }
            String remoteDBVersionString = stringBuilder.toString();
            remoteDBVersion = Float.parseFloat(remoteDBVersionString);

            // Get the current db version
            SharedPreferences sharedPreferences = MuchWord.getContext().getSharedPreferences(Constants.USER_PREFS, Context.MODE_PRIVATE);
            Float currentDBversion = sharedPreferences.getFloat(Constants.DB_VERSION, Constants.NOT_SET);

            // If the remote database version is higher, set dBUpdateAvailable to true
            if (remoteDBVersion> currentDBversion){
                Log.d(Constants.ALERT, "Remote DB version is " + remoteDBVersion + " current version is " + currentDBversion);
                dBUpdateAvailable = true;
            } else {
                Log.d(Constants.ALERT, "Remote DB version (" + remoteDBVersion + ") is same as current version ("
                        + currentDBversion + "), no update needed");
            }
        } catch(Exception e) {
            Log.d(Constants.ERROR, e.getMessage());
        }
        return null;
    }

    // After main execution, if a new database version is available, alert the user and
    // initiate a database download if the user provides a positive response
    @Override
    protected void onPostExecute(Long aLong) {
        super.onPostExecute(aLong);

        if (dBUpdateAvailable) {
            Log.d(Constants.ALERT, "Database update available");

            // Show an alert dialog notifying the user of the available update
            builder = new AlertDialog.Builder(currentActivity);
            builder.setTitle("Level updates available")
                    .setMessage("New levels are available. Would you like to download updates? Current progress will not be lost.")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            DownloadDBUpdate downloadDBUpdate = new DownloadDBUpdate(currentActivity, remoteDBVersion);
                            downloadDBUpdate.execute();
                            Log.d(Constants.ALERT, "Database download initiated");
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Log.d(Constants.ALERT, "User declined database update");
                        }
                    })
                    .show();
        }
    }
}
