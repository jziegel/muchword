package j_tan.muchword;

import android.app.Application;
import android.content.Context;

/**
 * Created by Ziggy on 11/5/2016.
 */

/* This class helps return a static context to be used in various parts of the app.
 * The code was adapated by user Rohit Ghatol's answer to a question on Stack Overflow.
 * References: Static way to get 'Context' on Android?
 *             http://stackoverflow.com/questions/2002288/static-way-to-get-context-on-android
 */
public class MuchWord extends Application {
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        MuchWord.context = getApplicationContext();
    }

    public static Context getContext() {
        return MuchWord.context;
    }
}
