package j_tan.muchword;

import android.app.ActivityManager;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/* This is the main game activity. It controls all aspects of the game, including gameplay, saving player progress
 * and scores, and creating level segue screens.
 */
public class GameActivity extends AppCompatActivity {

    private static int TILE_ROW_COUNT = 7;
    private static int TILE_COUNT = 14;
    private static int ANIMATION_DELAY = 200;
    private static int WORD_TILE_DIMENSION = 40;
    private static int WORD_TILE_MARGIN = 10;
    private static int ANSWER_TILE_DIMENSION = 25;
    private static int ANSWER_TILE_MARGIN = 5;
    private static int ALPHABET_LETTERS = 25;
    private ImageView imageViewImage1;
    private ImageView imageViewImage2;
    private TextView textViewScore;
    private LinearLayout linearLayoutRowOne;
    private LinearLayout linearLayoutRowTwo;
    private LinearLayout linearLayoutAnswerButtons;
    private SharedPreferences sharedPreferences;
    private DBHelper dbHelper;
    private Typeface customTypeface;
    private ArrayList<String> currentWordChars;
    private char[] currentAnswerChars;
    private float scale;
    private int currentWordCharCount;
    private int filledAnswerTileCount;
    private boolean[] answerTileFillStatus;
    private boolean isLevelCompleteDialogShowing = false;
    private boolean isTilesDisplayed = false;
    private boolean isMusicOn = false;
    private int currentScore;
    private int currentLevel;
    private Word currentWord;
    private MediaPlayer finishLevelSound;
    private MediaPlayer nextLevelSound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        Log.d(Constants.ALERT, "onCreate called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(Constants.ALERT, "onPause called");

        Intent intent = getIntent();
        intent.putExtra("answerTileFillStatus", answerTileFillStatus);
        intent.putExtra("filledAnswerTileCount", filledAnswerTileCount);
        intent.putExtra("currentAnswerChars", currentAnswerChars);
        intent.putExtra("isLevelCompleteDialogShowing", isLevelCompleteDialogShowing);
        intent.putExtra("isTilesDisplayed", isTilesDisplayed);
        intent.putExtra("isMusicOn", isMusicOn);

        // Stop music if it is playing
        if (isMusicPlaying()) {
            Log.d(Constants.ALERT, "Sound off");
            Intent music = new Intent(this, Music.class);
            stopService(music);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(Constants.ALERT, "onResume called");

        // Get the shared preferences file
        sharedPreferences = MuchWord.getContext().getSharedPreferences(Constants.USER_PREFS, Context.MODE_PRIVATE);

        // Get the current level from shared prefs
        currentLevel = sharedPreferences.getInt(Constants.CURRENT_LEVEL, Constants.NOT_SET);

        // Continue the game if the levels are not complete
        int numberOfLevels = sharedPreferences.getInt(Constants.NUMBER_OF_LEVELS, Constants.NOT_SET);

        // Create media players
        finishLevelSound = MediaPlayer.create(this, R.raw.glass);
        nextLevelSound = MediaPlayer.create(this, R.raw.tingsound);

        if (currentLevel <= numberOfLevels) {
            // Set up the custom action bar
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setCustomView(R.layout.actionbar_game);
                actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_CUSTOM);
            }

            // Set the image views for the word images
            imageViewImage1 = (ImageView) findViewById(R.id.imageViewImage1);
            imageViewImage2 = (ImageView) findViewById(R.id.imageViewImage2);
            linearLayoutRowOne = (LinearLayout) findViewById(R.id.linearLayoutRowOne);
            linearLayoutRowTwo = (LinearLayout) findViewById(R.id.linearLayoutRowTwo);
            linearLayoutAnswerButtons = (LinearLayout) findViewById(R.id.linearLayoutAnswerButtons);
            textViewScore = (TextView) findViewById(R.id.textViewScore);

            // Get the display density to calculate image sizes
            scale = getResources().getDisplayMetrics().density;

            // Create a custom typeface for use in the app
            customTypeface = Typeface.createFromAsset(getAssets(), "fonts/LeagueSpartan-Bold.otf");

            // Connect to the internal database
            try {
                dbHelper = new DBHelper(GameActivity.this);
            } catch (IOException e) {
                Log.d(Constants.ALERT, "Couldn't get database " + e.getMessage());
                e.printStackTrace();
            }
            continueGame();
        } else {
            showGameCompleteDialog();
        }

        // Start playing music with the music class
        if (getIntent().hasExtra("isMusicOn")) {
            if (getIntent().getBooleanExtra("isMusicOn", false)) { // If the music was previously on
                Log.d(Constants.ALERT, "Music was previously on");
                isMusicOn = true;
                Intent music = new Intent(this, Music.class);
                startService(music);
            } else { // If the music was previously off
                Log.d(Constants.ALERT, "Music was previously off");
                isMusicOn = false;
                Intent music = new Intent(this, Music.class);
                stopService(music);
                ImageButton soundButton = (ImageButton) findViewById(R.id.imageButtonSound);
                if (soundButton != null) {
                    Log.d(Constants.ALERT, "Got here");
                    soundButton.setImageResource(R.drawable.sound_on);
                }
            }
        } else { // If there is no previous state for music
            Log.d(Constants.ALERT, "No previous state for music");
            isMusicOn = true;
            getIntent().putExtra("isMusicOn", isMusicOn);
            Intent music = new Intent(this, Music.class);
            startService(music);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(Constants.ALERT, "onStop called");

        // Free up resources
        if (dbHelper != null) {
            dbHelper.close();
        }

        // Stop music if it is playing
        if (isMusicPlaying()) {
            Log.d(Constants.ALERT, "Sound off");
            Intent music = new Intent(this, Music.class);
            stopService(music);
        }
    }

    // Hides the action bar
    private void hideActionBar() {
        if (getSupportActionBar() != null && getSupportActionBar().isShowing()) {
            getSupportActionBar().hide();
        }
    }

    // Shows the action bar
    private void showActionBar() {
        if (getSupportActionBar() != null && !getSupportActionBar().isShowing()) {
            getSupportActionBar().show();
        }
    }

    // Main game handler method. Initiates variables, sets up views and continues the game from the last level
    private void continueGame() {
        Log.d(Constants.ALERT, "ContinueGame called");

        // Get the current level and score from shared prefs
        currentLevel = sharedPreferences.getInt(Constants.CURRENT_LEVEL, Constants.NOT_SET);
        currentScore = sharedPreferences.getInt(Constants.CURRENT_SCORE, Constants.NOT_SET);

        // If the current score is not set, this is a new game. Set the score to 0
        if (currentScore == Constants.NOT_SET) {
            currentScore = 0;
        }
        // If the current level is not set, this is a new game, set it to zero
        if (currentLevel == Constants.NOT_SET) {
            currentLevel = 0;
        }

        Log.d(Constants.ALERT, "Level " + String.valueOf(currentLevel) + " loaded from shared preferences");
        Log.d(Constants.ALERT, "Score " + String.valueOf(currentScore) + " loaded from shared preferences");

        // Display the score on the action bar
        String score = "Score: " + String.valueOf(currentScore);
        textViewScore.setText(score);

        // Get the word at the current level
        currentWord = getWord(currentLevel);

        // Get the current word char count
        currentWordCharCount = currentWord.getWord().length();

        // Get current word chars
        char[] currentWordCharArray = currentWord.getWord().toCharArray();
        currentWordChars = new ArrayList<>();
        for (int i = 0; i < currentWordCharArray.length; i++) {
            currentWordChars.add(String.valueOf(currentWordCharArray[i]));
        }

        Log.d(Constants.ALERT, "CurrentWordChars set to " + currentWordChars.toString());

        // Get intent to check for extras, in case user has resumed from the same level
        Intent intent = getIntent();

        // If the intent is not null, this mean the user may have resumed the game, check for extras
        if (intent == null) {
            Log.d(Constants.ALERT, "Intent is null");
        } else {
            // Filled answer tile count
            if (intent.hasExtra("filledAnswerTileCount")) {
                filledAnswerTileCount = intent.getIntExtra("filledAnswerTileCount", Constants.NOT_SET);
                Log.d(Constants.ALERT, "Got filledAnswerTileCount from intent, count: " + String.valueOf(filledAnswerTileCount));
            } else {
                filledAnswerTileCount = 0;
                Log.d(Constants.ALERT, "FlledAnswerTileCount set to 0");
            }

            // Answer tile fill status
            if (intent.hasExtra("answerTileFillStatus")) {
                if (intent.getBooleanArrayExtra("answerTileFillStatus") != null) {
                    answerTileFillStatus = intent.getBooleanArrayExtra("answerTileFillStatus");
                } else {
                    answerTileFillStatus = new boolean[currentWordCharCount];
                }
                Log.d(Constants.ALERT, "Got answerTileFillStatus from intent");
            } else {
                answerTileFillStatus = new boolean[currentWordCharCount];
                Log.d(Constants.ALERT, "AnswerTileFillStatus set to new");
            }

            // Current answer chars
            if (intent.hasExtra("currentAnswerChars")) {
                if (intent.getCharArrayExtra("currentAnswerChars") != null) {
                    currentAnswerChars = intent.getCharArrayExtra("currentAnswerChars");
                } else {
                    currentAnswerChars = new char[currentWordCharCount];
                    for (char c : currentAnswerChars) {
                        c = ' ';
                    }
                }
                Log.d(Constants.ALERT, "Got currentAnswerChars from intent");
            } else {
                currentAnswerChars = new char[currentWordCharCount];
                for (char c : currentAnswerChars) {
                    c = ' ';
                }
            }
            if (intent.hasExtra("isLevelCompleteDialogShowing")) {
                isLevelCompleteDialogShowing = intent.getBooleanExtra("isLevelCompleteDialogShowing", false);
                Log.d(Constants.ALERT, "Got isLevelCompleteDialogShowing from intent, status: " + String.valueOf(isLevelCompleteDialogShowing));
            }
        }

        // If the tiles are not displayed already, display them
        if (!isLevelCompleteDialogShowing && !isTilesDisplayed) {
            setAnswerTiles(currentWord);
            setWordImages(currentWord);
            setWordLetterTiles(currentWord);
            isTilesDisplayed = true;
        }
    }

    // Finishes the current level, saves scores, removes views
    private void finishLevel() {
        Log.d(Constants.ALERT, "Level complete");

        // Play the level end sound
        playSound(finishLevelSound);

        // Increment score and save it
        currentScore += 30;
        Log.d(Constants.ALERT, "Current score increased to " + String.valueOf(currentScore));
        saveScore(currentScore);

        // Increment the current level and save it
        ++currentLevel;
        Log.d(Constants.ALERT, "Current level incremented to " + String.valueOf(currentLevel));
        saveLevel(currentLevel);

        // Remove child answer tiles and letter tiles
        int answerTilesCount = linearLayoutAnswerButtons.getChildCount();
        int letterTileRowOneCount = linearLayoutRowOne.getChildCount();
        int letterTileRowTwoCount = linearLayoutRowTwo.getChildCount();
        for (int i = 0; i < answerTilesCount; i++) {
            Button answerTile = (Button) linearLayoutAnswerButtons.getChildAt(i);
            tileDisappear(answerTile);
        }
        for (int i = 0; i < letterTileRowOneCount; i++) {
            ImageButton letterTile = (ImageButton) linearLayoutRowOne.getChildAt(i);
            tileDisappear(letterTile);
        }
        for (int i = 0; i < letterTileRowTwoCount; i++) {
            ImageButton letterTile = (ImageButton) linearLayoutRowTwo.getChildAt(i);
            tileDisappear(letterTile);
        }

        // Set tracking variable
        isTilesDisplayed = false;
        getIntent().putExtra("isTilesDisplayed", isTilesDisplayed);

        // Animate out images
        imageDisappear(imageViewImage1);
        imageDisappear(imageViewImage2);

        // Show level complete after giving some time for animation to complete
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                showLevelCompleteDialog();
            }
        }, 250);
    }

    // Plays the given sound of music is not muted
    private void playSound(MediaPlayer mediaPlayer) {
        if (isMusicOn) {
            mediaPlayer.start();
        }
    }

    /* Shows a level complete dialog with the current score
     * References: DialogFragment on Android Developers
     *             https://developer.android.com/reference/android/app/DialogFragment.html#BasicDialog
     */
    private void showLevelCompleteDialog() {
        Log.d(Constants.ALERT, "Showing level complete dialog");
        // Hide the actionbar_game
        hideActionBar();

        // Set tracking variable to true
        isLevelCompleteDialogShowing = true;

        // Remove any previously showing dialogs
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();

        // Show a new dialog fragment with the current score
        DialogFragment dialogFragment = LevelCompleteDialog.levelComplete(currentScore);
        dialogFragment.show(fragmentTransaction, LevelCompleteDialog.TAG);
    }

    private void showGameCompleteDialog() {
        Log.d(Constants.ALERT, "Showing game complete dialog");
        // Hide the actionbar_game
        hideActionBar();

        // Remove any previously showing dialogs
        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();

        // Show a new game complete dialog fragment
        DialogFragment dialogFragment = GameCompleteDialog.gameComplete();
        dialogFragment.show(fragmentTransaction, GameCompleteDialog.TAG);
    }

    // Cleans up variables and starts next level
    public void goToNextLevel() {
        Log.d(Constants.ALERT, "Going to next level");

        // Play the next level sound
        playSound(nextLevelSound);

        // Clear all views
        linearLayoutAnswerButtons.removeAllViews();
        linearLayoutRowOne.removeAllViews();
        linearLayoutRowTwo.removeAllViews();

        // Set objects to null
        currentWord = null;
        currentWordChars = null;
        answerTileFillStatus = null;
        currentAnswerChars = null;

        // Set counts to zero
        filledAnswerTileCount = 0;
        currentWordCharCount = 0;

        // Dialog has been dismissed
        isLevelCompleteDialogShowing = false;

        // Save the info in intent, these will be checked when continueGame is called
        Intent intent = getIntent();
        intent.putExtra("filledAnswerTileCount", filledAnswerTileCount);
        intent.putExtra("isLevelCompleteDialogShowing", isLevelCompleteDialogShowing);
        intent.putExtra("answerTileFillStatus", answerTileFillStatus);
        intent.putExtra("currentAnswerChars", currentAnswerChars);

        int numberOfLevels = sharedPreferences.getInt(Constants.NUMBER_OF_LEVELS, Constants.NOT_SET);
        if (currentLevel <= numberOfLevels) {
            Log.d(Constants.ALERT, "Continuing to level " + String.valueOf(currentLevel) + " of " + String.valueOf(numberOfLevels));
            showActionBar();
            continueGame();
        } else {
            showGameCompleteDialog();
        }
    }

    // Method to set the given word's images on the game image views
    private void setWordImages(Word word) {
        // Set the word's images into the two imageviews, with a slightly delayed animation
        if (imageViewImage1 != null) {
            imageViewImage1.setImageBitmap(word.getImage1());
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    imageAppear(imageViewImage1);
                }
            }, 400);
        }
        if (imageViewImage2 != null) {
            imageViewImage2.setImageBitmap(word.getImage2());
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    imageAppear(imageViewImage2);
                }
            }, 800);
        }
    }

    // Method to set answer tiles for the characters of the given word
    private void setAnswerTiles(Word word) {
        int answerTileCount = word.getWord().length();
        for (int i = 0; i < answerTileCount; i++) {
            Button answerTile = createAnswerTile();
            linearLayoutAnswerButtons.addView(answerTile);
            tileAppear(answerTile);
        }
    }

    // Create an answer tile button
    private Button createAnswerTile() {
        Button answerTile = new Button(GameActivity.this);
        int drawableID = getResources().getIdentifier("answer", "drawable", getPackageName());
        answerTile.setBackgroundResource(drawableID);
        answerTile.setLayoutParams(getTileDimension(ANSWER_TILE_DIMENSION, ANSWER_TILE_MARGIN));
        answerTile.setPadding(0, 0, 0, 0);
        answerTile.setTypeface(customTypeface);
        answerTile.setEnabled(false);
        return answerTile;
    }

    // Method to get the next word from the database
    private Word getWord(int currentLevel) {
        return dbHelper.getWord(currentLevel);
    }

    // Method to get the characters of the word and mix them with random characters and create a view list
    private void setWordLetterTiles(Word word) {
        // Get the characters from the current word
        char characters[] = word.getWord().toCharArray();

        // Create a list of imagebuttons for all the characters in the word
        List<ImageButton> imageButtonList = new ArrayList<ImageButton>();
        for (char c : characters
                ) {
            ImageButton imageButton = createWordLetterTile(c);
            imageButtonList.add(imageButton);
        }

        // Calculate how many random character imagebuttons are needed to make a total of 14
        int randomTileCount = TILE_COUNT - imageButtonList.size();

        // Create imagebuttons with random characters and add them to the list
        for (int i = 0; i < randomTileCount; i++) {
            imageButtonList.add(createRandomLetterTile());
        }

        // Shuffle the imagebutton list
        Collections.shuffle(imageButtonList);

        // Split the imagebuttonlist into two lists, one for each row
        final List<ImageButton> imageButtonsRowOne = new ArrayList<ImageButton>();
        final List<ImageButton> imageButtonsRowTwo = new ArrayList<ImageButton>();
        for (int i = 0; i < TILE_ROW_COUNT; i++) {
            imageButtonsRowOne.add(imageButtonList.get(i));
        }
        for (int i = TILE_ROW_COUNT; i < TILE_COUNT; i++) {
            imageButtonsRowTwo.add(imageButtonList.get(i));
        }

        // Add the imagebuttons to the respective linear layout with animation
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                animateAppearLetterTiles(linearLayoutRowOne, imageButtonsRowOne, ANIMATION_DELAY);
                animateAppearLetterTiles(linearLayoutRowTwo, imageButtonsRowTwo, ANIMATION_DELAY);
            }
        }, 1200);

    }

    // Method to create a letter tile image button for the given char
    private ImageButton createWordLetterTile(char c) {
        // Create an image button
        ImageButton imageButton = new ImageButton(GameActivity.this);

        // Set the drawable accoring to the letter
        int drawableID = getResources().getIdentifier("letter_" + c, "drawable", getPackageName());
        imageButton.setImageResource(drawableID);

        // Set the button's parameters, layout and onclick listener
        imageButton.setLayoutParams(getTileDimension(WORD_TILE_DIMENSION, WORD_TILE_MARGIN));
        imageButton.setScaleType(ImageView.ScaleType.FIT_XY);
        imageButton.setPadding(0, 0, 0, 0);
        imageButton.setVisibility(View.INVISIBLE);
        imageButton.setOnClickListener(letterTileOnClickListener(c));
        return imageButton;
    }

    // Method to return an image button tile with a drawable for a random char
    private ImageButton createRandomLetterTile() {
        // Create a new image button
        ImageButton imageButton = new ImageButton(GameActivity.this);

        // Find a random character from the alphabet
        Random random = new Random();
        int letterNumber = random.nextInt(ALPHABET_LETTERS);
        char[] alphabet = new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'x', 'y', 'z'};
        char selectedChar = alphabet[letterNumber];

        // Set the imabe button's image to that random character
        int drawableID = getResources().getIdentifier("letter_" + selectedChar, "drawable", getPackageName());
        imageButton.setImageResource(drawableID);
        imageButton.setLayoutParams(getTileDimension(WORD_TILE_DIMENSION, WORD_TILE_MARGIN));
        imageButton.setScaleType(ImageView.ScaleType.FIT_XY);
        imageButton.setPadding(0, 0, 0, 0);
        imageButton.setVisibility(View.INVISIBLE);

        // Set the button's on click listener
        imageButton.setOnClickListener(letterTileOnClickListener(selectedChar));
        return imageButton;
    }

    private View.OnClickListener letterTileOnClickListener(final char character) {
        return new View.OnClickListener() {
            @Override
            public void onClick(final View letterTile) {
                // If the answer tiles are not full
                if (filledAnswerTileCount != currentWordCharCount) {
                    // Disable the tile so it can't receive anymore click events
                    letterTile.setEnabled(false);
                    Log.d(Constants.ALERT, "Letter '" + character + "' was clicked");
                    // If the current word contains the character
                    if (currentWordChars.contains(character)) {
                        // Get its location in the current word character list and remove it
                        int location = currentWordChars.indexOf(character);
                        currentWordChars.remove(location);
                        Log.d(Constants.ALERT, "Letter '" + character + "' was found in word at index " + location + " and removed");
                        // Make the tile disappear
                        tileDisappear(letterTile);
                        Log.d(Constants.ALERT, "Remaining chars in word = " + currentWordChars.toString());
                    } else {
                        // Else, just remove the tile
                        tileDisappear(letterTile);
                        Log.d(Constants.ALERT, "Letter '" + character + "' was removed");
                    }
                    Log.d(Constants.ALERT, "Filled answer tile count = " + String.valueOf(filledAnswerTileCount) + ", current word char count = " + String.valueOf(currentWordCharCount));
                    // Get the position of the next free answer tile
                    final int answerTilePosition = getNextFreeAnswerTilePosition();
                    // Get the answerTile at that position, add the character to it
                    final Button answerTile = (Button) linearLayoutAnswerButtons.getChildAt(answerTilePosition);
                    answerTile.setEnabled(true);
                    answerTile.setText(String.valueOf(character));
                    // Set the answer tile position as full
                    setAnswerTileFull(answerTilePosition, character);
                    // Set an onClickListener for the answer tile answerTile
                    answerTile.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            answerTile.setEnabled(false);
                            // If the answerTile is clicked, remove its text
                            answerTile.setText("");
                            // Make the letter tile that triggered this answer tile's activation to re-appear
                            tileAppear(letterTile);
                            letterTile.setEnabled(true);
                            // Mark this answer tile as empty
                            setAnswerTileEmpty(answerTilePosition);
                            //currentAnswerChars.remove(answerTilePosition);
                        }
                    });
                    // If all the answer tiles are full
                    if (filledAnswerTileCount == currentWordCharCount) {
                        Log.d(Constants.ALERT, "Filled answer count is equal to current word char count");
                        // Check if the level has been completed
                        if (isLevelComplete()) {
                            // Finish the level
                            finishLevel();
                        } else {
                            Log.d(Constants.ALERT, "Incorrect answer entered");
                        }
                    }
                }
            }
        };
    }

    // Sets a given answer tile as full, sets the character at the given position
    private void setAnswerTileFull(int location, char character) {
        // Set the two answer tracking variables
        answerTileFillStatus[location] = true;
        currentAnswerChars[location] = character;
        // Increment the filled answer tile count
        ++filledAnswerTileCount;
        Log.d(Constants.ALERT, "Answer tile location: " + String.valueOf(location) + " has been set to full");
        Log.d(Constants.ALERT, "Current answer tiles: ");
        System.out.println(currentAnswerChars);
    }

    // Sets an answer tile as empty at the given position
    private void setAnswerTileEmpty(int location) {
        // Set the two answer tracking variables
        answerTileFillStatus[location] = false;
        currentAnswerChars[location] = ' ';
        // Decrement the filled answer tile count
        --filledAnswerTileCount;
        Log.d(Constants.ALERT, "Answer tile location: " + String.valueOf(location) + " has been set to empty");
        Log.d(Constants.ALERT, "Current answer tiles: ");
        System.out.println(currentAnswerChars);
    }

    // Checks whether the entered answer is correct
    private boolean isLevelComplete() {
        char[] currentWordChars = currentWord.getWord().toCharArray();
        boolean compare = true;

        // Check each character in the current answer against the current word
        if (filledAnswerTileCount == currentWordCharCount) {
            for (int i = 0; i < currentWordCharCount; i++) {
                if (currentAnswerChars[i] != currentWordChars[i]) {
                    compare = false;
                }
            }
        }
        return compare;
    }

    // Returns the position of the next free answer tile
    private int getNextFreeAnswerTilePosition() {
        int nextFreeAnswerTilePosition = 0;
        boolean nextFreePositionFound = false;

        // Incrementally check the answer tiles for a free tile
        while (!nextFreePositionFound) {
            // If the answer tile is not filled, and all the answer tiles are not full
            if (!answerTileFillStatus[nextFreeAnswerTilePosition] && (filledAnswerTileCount != currentWordCharCount)) {
                nextFreePositionFound = true;
            } else if (filledAnswerTileCount != currentWordCharCount) {
                ++nextFreeAnswerTilePosition;
            }
        }
        Log.d(Constants.ALERT, "Next free answer tile position: " + String.valueOf(nextFreeAnswerTilePosition));
        return nextFreeAnswerTilePosition;
    }

    // Method to save the current level
    private void saveLevel(int currentLevel) {
        sharedPreferences.edit().putInt(Constants.CURRENT_LEVEL, currentLevel).apply();
        Log.d(Constants.ALERT, "Level " + String.valueOf(currentLevel) + " saved");
    }

    // Method to save the current score
    private void saveScore(int currentScore) {
        sharedPreferences.edit().putInt(Constants.CURRENT_SCORE, currentScore).apply();
        Log.d(Constants.ALERT, "Score " + String.valueOf(currentScore) + " saved");
    }

    /* Method to get block dimensions
     * The code to calculate the screen density was adapted from an answer to a post by user sham on Stack Overflow.
     * References: How can we set height and width dp for imageview in android?
     *             http://stackoverflow.com/questions/20948793/how-can-we-set-height-and-width-dp-for-imageview-in-android
     */
    private LinearLayout.LayoutParams getTileDimension(int tileDimension, int tileMargin) {
        // Calculate the images width and height based on the screen density
        int width = (int) (tileDimension * scale);
        int height = (int) (tileDimension * scale);

        // Create a layout params
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, height);
        layoutParams.rightMargin = tileMargin;
        layoutParams.leftMargin = tileMargin;
        return layoutParams;
    }

    // Method to animate-in letter tiles into the given linear layout
    private void animateAppearLetterTiles(final LinearLayout linearLayout, List<ImageButton> imageButtonList, int animationDelay) {
        // Add the image buttons to the linear layout
        for (int i = 0; i < imageButtonList.size(); i++) {
            final ImageButton imageButton = imageButtonList.get(i);
            linearLayout.addView(imageButton);
        }
        // Animate in each image button
        for (int i = 0; i < linearLayout.getChildCount(); i++) {
            int delay = i * animationDelay;
            final ImageButton imageButton = (ImageButton) linearLayout.getChildAt(i);
            imageButton.postDelayed(new Runnable() {
                @Override
                public void run() {
                    tileAppear(imageButton);
                }
            }, delay);
        }
    }

    /* Animations
     * References: View Animation on Android Developers
     *             https://developer.android.com/guide/topics/graphics/view-animation.html
     */

    // Make tile appear
    private void tileAppear(View view) {
        Animation appearAnimation = AnimationUtils.loadAnimation(GameActivity.this, R.anim.letter_appear);
        view.setVisibility(View.VISIBLE);
        view.startAnimation(appearAnimation);
    }

    // Make tile disappear
    private void tileDisappear(View view) {
        Animation disappearAnimation = AnimationUtils.loadAnimation(GameActivity.this, R.anim.letter_disappear);
        view.startAnimation(disappearAnimation);
        view.setVisibility(View.INVISIBLE);
    }

    // Make image appear
    private void imageAppear(View view) {
        Animation appearAnimation = AnimationUtils.loadAnimation(GameActivity.this, R.anim.image_appear);
        view.setVisibility(View.VISIBLE);
        view.startAnimation(appearAnimation);
    }

    // Make image disappear
    private void imageDisappear(View view) {
        Animation disappearAnimation = AnimationUtils.loadAnimation(GameActivity.this, R.anim.image_disappear);
        view.startAnimation(disappearAnimation);
        view.setVisibility(View.INVISIBLE);
    }

    // Make an alert to show a hint of answer
    void showHintDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this);
        builder.setTitle("Choose a hint")
                .setItems(R.array.levels_array, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                setHintOption(1);
                                break;
                            case 1:
                                setHintOption(2);
                                break;
                            case 2:
                                setHintOption(3);
                                break;
                            default:
                                break;
                        }
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    // Show hint options
    void setHintOption(int level) {
        String hintLetters = currentWord.getWord();

        String last = hintLetters.substring(hintLetters.length() - 1);
        String first = hintLetters.substring(0, 1);
        String third = hintLetters.substring(hintLetters.length() - 2);

        AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this);

        switch (level) {
            case 1:
                builder.setTitle("Hint 1")
                        .setMessage("The word starts with '" + first + "'");
                builder.show();

                break;
            case 2:
                builder.setTitle("Hint 2")
                        .setMessage("The word starts with '" + first + "' and ends with '" + last + "'");
                builder.show();
                break;
            case 3:
                builder.setTitle("Hint 3")
                        .setMessage("The word starts with '" + first + "' and ends with '" + third + "'");
                builder.show();
                break;
        }
    }

    // Finish the activity when the back button is clicked
    public void onBackButtonClick(View view) {
        finish();
    }

    // Controls the sound button function
    public void onSoundButtonClick(View view) {
        Intent music = new Intent(this, Music.class);
        ImageButton soundButton = (ImageButton) view;

        // If music is playing, stop it, else start it
        if (!isMusicPlaying()) {
            Log.d(Constants.ALERT, "Sound on");
            startService(music);
            // Set the image resource to sound off
            soundButton.setImageResource(R.drawable.sound_off);
            isMusicOn = true;
        } else {
            Log.d(Constants.ALERT, "Sound off");
            stopService(music);
            isMusicOn = false;

            // Set the image resource to sound on
            soundButton.setImageResource(R.drawable.sound_on);
        }
    }

    // Show the hint dialog when the hint button is clicked
    public void onHintButtonClick(View view) {
        showHintDialog();
    }

    /* Checks if music is already playing
     * The code to check if a service is already running was adapted to an answer to a post by user geekQ on Stack Overflow
     * References: How to check if a service is running on Android?
     *             http://stackoverflow.com/questions/600207/how-to-check-if-a-service-is-running-on-android
     */
    private boolean isMusicPlaying() {
        boolean isPlaying = false;
        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo serviceInfo : activityManager.getRunningServices(Integer.MAX_VALUE)) {
            if (Music.class.getName().equals(serviceInfo.service.getClassName())) {
                isPlaying = true;
            }
        }
        return isPlaying;
    }

    // Cleans up and finishes the activity
    public void finishActivity() {
        // Close the db connection
        if (dbHelper != null) {
            dbHelper.close();
        }

        // Stop music if it is playing
        if (isMusicPlaying()) {
            Log.d(Constants.ALERT, "Sound off");
            Intent music = new Intent(GameActivity.this, Music.class);
            stopService(music);
        }
        finish();
    }
}
