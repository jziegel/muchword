package j_tan.muchword;

/**
 * Created by Ziggy on 11/5/2016.
 */

/* This class contains all the constants used by the app, for easy access and consistency.
 */
public class Constants {
    // Shared preference keys
    public static final String USER_PREFS = "USER_PREFS";
    public static final String DB_VERSION = "USER_DB_VERSION";
    public static final String CURRENT_LEVEL = "CURRENT_LEVEL";
    public static final String CURRENT_SCORE = "CURRENT_SCORE";
    public static final String APP_VERSION = "APP_VERSION";
    public static final String NUMBER_OF_LEVELS = "LEVEL_COUNT";

    // Default app data
    public static final float LOADED_DB_VERSION = 1.0f;
    public static final int DEFAULT_LEVEL = 1;
    public static final int NOT_SET = -1;
    public static final int DEFAULT_SCORE = 0;

    // Database info
    public static final String DB_STORAGE_PATH = MuchWord.getContext().getFilesDir().toString() + "/worddb.db";
    public static final String DB_URL = "https://s3-ap-southeast-2.amazonaws.com/muchword/worddb.db";
    public static final String DATABASE_NAME = "worddb.db";

    // Debugger
    public static final String ALERT = "APP ALERT: ";
    public static final String ERROR = "APP ERROR: ";
}
