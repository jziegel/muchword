package j_tan.muchword;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Exchanger;

/* DBHelper Class
 * Connects to the database and manages it. Public methods provide tools to check the database's existence and copy the
 * database from the app folder on first run.
 */
public class DBHelper extends SQLiteOpenHelper {

    // Word table details
    public static final String WORD_TABLE_NAME = "words";
    public static final String COLUMN_WORD = "word";
    public static final String COLUMN_IMAGE_1 = "image1";
    public static final String COLUMN_IMAGE_2 = "image2";

    // Quote table details
    public static final String QUOTE_TABLE_NAME = "quotes";
    public static final String COLUMN_QUOTE = "quote";

    private static final int DATABASE_VERSION = 1;

    public DBHelper(Context activityContext) throws IOException {
        super(activityContext, Constants.DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Nothing to do here
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Database upgrade is handled by a separate class
    }

    @Override
    public SQLiteDatabase getReadableDatabase() {
        // Use the internal database
        String databaseFile = (Constants.DB_STORAGE_PATH);
        return SQLiteDatabase.openDatabase(databaseFile, null, SQLiteDatabase.OPEN_READONLY);
    }

    // Returns a word object at the specified level, containing a word string, and two bitmap images
    public Word getWord(int level) {
        // Get a readable database
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();

        // Create a string of the level to query the database with
        String levelID = String.valueOf(level);

        // A word object to be returned
        Word word = new Word();

        // Cursor to retrieve values from the column row
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + WORD_TABLE_NAME + " WHERE id=?", new String[]{levelID});

        // Strings to store data retrieved from the database
        String wordString;
        String wordImage1;
        String wordImage2;

        // If the cursor is not empty, add data to the word object
        if (cursor.getCount() > 0) {
            // Move the cursor to the first and only row
            cursor.moveToFirst();

            // Get the word string from the database and add it to the word object
            wordString = cursor.getString(cursor.getColumnIndex(COLUMN_WORD));
            word.setWord(wordString);

            // Get the base64 encoded images from the database
            wordImage1 = cursor.getString(cursor.getColumnIndex(COLUMN_IMAGE_1));
            wordImage2 = cursor.getString(cursor.getColumnIndex(COLUMN_IMAGE_2));

            /* This code to decode base64 images to bitmaps was adapted from user432209's answer to a post on Stack Overflow
             * References: How to convert a Base64 string into a BitMap image to show it in a ImageView? -
             *             http://stackoverflow.com/questions/4837110/how-to-convert-a-base64-string-into-a-bitmap-image-to-show-it-in-a-imageview
             */
            // Decode the image strings into byte arrays
            byte[] decodedImage1 = Base64.decode(wordImage1, Base64.DEFAULT);
            byte[] decodedImage2 = Base64.decode(wordImage2, Base64.DEFAULT);

            // Create bitmaps from the byte arrays and add them to the word object
            word.setImage1(BitmapFactory.decodeByteArray(decodedImage1, 0, decodedImage1.length));
            word.setImage2(BitmapFactory.decodeByteArray(decodedImage2, 0, decodedImage2.length));

            /* End user432209's code */

            // Free up resources, close connections
            cursor.close();
            sqLiteDatabase.close();
        }
        Log.d(Constants.ALERT, "Word " + word.getWord() + " selected from database");
        return word;
    }

    public String getRandomQuote() {
        // Get a readable database
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();

        // A list to store the quotes from the database and a string to store the selected quote
        List<String> quotes = new ArrayList<>();
        String quote;

        // Cursor to retrieve values from the column row
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + QUOTE_TABLE_NAME, null);
        while (cursor.moveToNext()) {
            quotes.add(cursor.getString(cursor.getColumnIndex(COLUMN_QUOTE)));
        }

        // Select a random quote from the list
        Random random = new Random();
        int selectedQuote = random.nextInt(quotes.size());
        quote = quotes.get(selectedQuote);

        Log.d(Constants.ALERT, "Quote '" + quote + "' selected from database");

        // Free up resources
        cursor.close();
        sqLiteDatabase.close();
        return quote;
    }

    // Updates the number of levels stored in shared prefs
    public static void updateNumberOfLevels() {
        int numberOfLevels = 0;

        try {
            DBHelper dbHelper = new DBHelper(MuchWord.getContext());
            // Get a readable database
            SQLiteDatabase sqLiteDatabase = dbHelper.getReadableDatabase();

            // Cursor to retrieve rows
            Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + WORD_TABLE_NAME, null);
            while (cursor.moveToNext()) {
                ++numberOfLevels;
            }

            // Update the number of levels on shared prefs
            SharedPreferences sharedPreferences = MuchWord.getContext().getSharedPreferences(Constants.USER_PREFS, Context.MODE_PRIVATE);
            sharedPreferences.edit().putInt(Constants.NUMBER_OF_LEVELS, numberOfLevels).apply();

            Log.d(Constants.ALERT, "Number of levels set to " + String.valueOf(numberOfLevels));

            // Free up resources
            cursor.close();
            sqLiteDatabase.close();

        } catch (Exception e) {
            Log.d(Constants.ERROR, "Couldn't update number of levels");
        }
    }

    /* This code to check if a database file exists / copy a database file from the app directory to storage was written by
     * following a tutorial by user Juan-Manuel Fluxa on the reigndesign.com blog.
     * References: Using your own SQLite database in Android applications
     *             http://blog.reigndesign.com/blog/using-your-own-sqlite-database-in-android-applications/
     */
    public static boolean checkDatabaseExists() {
        File database = new File(Constants.DB_STORAGE_PATH);
        Log.d(Constants.ALERT, "Database existence check returned " + database.exists());
        return database.exists();
    }

    public static void copyDatabase() throws IOException {
        // An input stream to read the internal database and output stream to save it to storage
        InputStream inputStream = MuchWord.getContext().getApplicationContext().getAssets().open(Constants.DATABASE_NAME);
        OutputStream outputStream = new FileOutputStream(Constants.DB_STORAGE_PATH);
        byte[] buffer = new byte[1024];
        int length;

        // Write the inputstream to outputstream
        while ((length = inputStream.read(buffer)) > 0) {
            outputStream.write(buffer, 0, length);
        }

        // Finish and free up resources
        outputStream.flush();
        outputStream.close();
        inputStream.close();
        Log.d(Constants.ALERT, "Database copied to internal storage location " + Constants.DB_STORAGE_PATH);
    }
}


