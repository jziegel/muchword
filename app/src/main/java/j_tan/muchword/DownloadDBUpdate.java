package j_tan.muchword;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Ziggy on 5/5/2016.
 */

/* This class downloads and updates the current game database. It shows a download progress dialog while downloading the database.
 * The code to download a file and show progress was created by follwing a tutorial by user Team Quick Tips on QuickTips.in
 * References: Show ProgressDialog Android - http://www.quicktips.in/show-progressdialog-android/
 *             AsyncTask on Android Developers - http://developer.android.com/reference/android/os/AsyncTask.html
 *             HttpURLConnection on Android Developers - http://developer.android.com/reference/java/net/HttpURLConnection.html
 */

public class DownloadDBUpdate extends AsyncTask<URL, Integer, Long>{
    public DownloadDBUpdate(Activity activity, Float dBVersion){
        currentActivity = activity;
        remoteDBVersion = dBVersion;
    }

    // A progress dialog to show the database download progress
    ProgressDialog downloadProgressDialog;

    // The current activity is needed to successfully create a progress dialog
    Activity currentActivity;

    // Keep track of the remote db version and save in shared prefs on successful db update
    Float remoteDBVersion;

    // Create a progress dialog
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // Set the parameters for the progress dialog
        downloadProgressDialog = new ProgressDialog(currentActivity);
        downloadProgressDialog.setTitle("Updating Database");
        downloadProgressDialog.setMessage("Please wait...");
        downloadProgressDialog.setMax(100);
        downloadProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        downloadProgressDialog.setCancelable(false);
        downloadProgressDialog.show();
    }

    // Download the database file from the amazon s3 bucket
     /* References: HttpURLConnection on Android Developer - http://developer.android.com/reference/java/net/HttpURLConnection.html
     *              Connecting to the Network on Android Developer - http://developer.android.com/training/basics/network-ops/connecting.html
     *              Saving Files on Android Developer - https://developer.android.com/training/basics/data-storage/files.html
     */
    @Override
    protected Long doInBackground (URL... databaseUrl) {
        try {
            // Create a new connection to the S3 bucket db url
            URL url = new URL(Constants.DB_URL);
            HttpURLConnection httpURLconnection = (HttpURLConnection) url.openConnection();

            // Input stream to read the db and output stream to write it
            InputStream inputStream = new BufferedInputStream(httpURLconnection.getInputStream());
            OutputStream outputStream = new FileOutputStream(Constants.DB_STORAGE_PATH);
            int fileLength = httpURLconnection.getContentLength();
            byte[] buffer = new byte[1024];
            int bytesRead;
            long total = 0;

            // Write input stream to output stream
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
                total += bytesRead;
                publishProgress((int)((total * 100) / fileLength));
            }

            // Finish up
            outputStream.flush();
            inputStream.close();
            outputStream.close();
            httpURLconnection.disconnect();
            Log.d(Constants.ALERT, "Database update complete");

            // Update the current db version on shared prefs
            SharedPreferences sharedPreferences = MuchWord.getContext().getSharedPreferences(Constants.USER_PREFS, Context.MODE_PRIVATE);
            sharedPreferences.edit().putFloat(Constants.DB_VERSION, remoteDBVersion).apply();
            Log.d(Constants.ALERT, "DB version set to " + remoteDBVersion.toString());

            // Update the number of levels in the new db in shard prefs
            DBHelper.updateNumberOfLevels();
        } catch (Exception e) {
            Log.d(Constants.ERROR, "Couldn't update database");
            Log.d(Constants.ERROR, e.getMessage());
        }
        return null;
    }

    // Update the progress dialog while downloading
    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        downloadProgressDialog.setProgress(values[0]);
    }

    // Dismiss the progress dialog after execution
    @Override
    protected void onPostExecute(Long aLong) {
        super.onPostExecute(aLong);
        if (downloadProgressDialog != null) {
            if (downloadProgressDialog.isShowing()) {
                downloadProgressDialog.dismiss();
            }
        }
    }
}
