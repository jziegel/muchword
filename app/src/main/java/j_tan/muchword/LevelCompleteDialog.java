package j_tan.muchword;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;

/**
 * Created by Ziggy on 16/5/2016.
 */

/* A dialog fragment to notify the user that the level is complete. It will show the current score
 * a random quote from the db.
 * References: DialogFragment on Android Developers
 *             https://developer.android.com/reference/android/app/DialogFragment.html
 */
public class LevelCompleteDialog extends DialogFragment {

    public static String TAG = "Level Complete Dialog";
    int levelScore;

    static LevelCompleteDialog levelComplete(int levelScore) {
        LevelCompleteDialog ls = new LevelCompleteDialog();
        Bundle bundle = new Bundle();
        bundle.putInt("levelScore", levelScore);
        ls.setArguments(bundle);
        return ls;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        /* Animation for this class was created by adapting code from a an answer to a post by user Xavi Gil on Stack Overflow.
         * References: Show DialogFragment with animation growing from a point
         *             http://stackoverflow.com/questions/13402782/show-dialogfragment-with-animation-growing-from-a-point
         */
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        getDialog().getWindow().setBackgroundDrawableResource(R.color.transparent);
    }

    // Get the score stored in the bundle for display
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        levelScore = getArguments().getInt("levelScore");
    }

    /* Use onCancel, as onDismiss is called when the fragment is popped from the stack.
     * When cancelled, the game should advance to the next level
     */
    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        Log.d(Constants.ALERT, "Level Complete Dialog cancelled");
        ((GameActivity) getActivity()).goToNextLevel();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Set the layout
        View view = inflater.inflate(R.layout.level_complete_dialog_fragment, container, false);

        // Default quote
        String quote = "You did great!";

        // Get a random quote from the database
        try {
            DBHelper dbHelper = new DBHelper(MuchWord.getContext());
            quote = dbHelper.getRandomQuote();
        } catch (IOException e) {
            Log.d(Constants.ERROR, "Couldn't get random quote");
            e.printStackTrace();
        }

        // Display the quote
        TextView textViewQuote = (TextView) view.findViewById(R.id.textViewQuote);
        textViewQuote.setText(quote);

        // Show the current score
        String score = "Current score: " + levelScore;
        TextView textViewLevelSegue = (TextView) view.findViewById(R.id.textViewScore);
        textViewLevelSegue.setText(score);

        // Create custom typeface for button text and apply to buttons
        Typeface customTypeface = Typeface.createFromAsset(MuchWord.getContext().getAssets(), "fonts/LeagueSpartan-Bold.otf");
        Button buttonNextLevel = (Button) view.findViewById(R.id.buttonNextLevel);
        buttonNextLevel.setTypeface(customTypeface);

        // Cancel the dialog on click
        buttonNextLevel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().cancel();
            }
        });
        return view;
    }
}
