package j_tan.muchword;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.IOException;

/* This is the main menu class. It handles checking permissions, updating the database,
 * starting a new game, viewing the about activity.
 */
public class MenuActivity extends AppCompatActivity {

    // Request permission callback codes
    private final int NETWORK_STATE = 0;
    private final int INTERNET = 1;

    // DBUpdater to be called to check for db updates
    private DBUpdater DBUpdater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        // Hide the action bar
        hideActionBar();

        // Check if this is the app's first run
        checkFirstRun();

        // Create a DBUpdater and check permissions
        DBUpdater = new DBUpdater(MenuActivity.this);
        checkPermissions();

        // Style interface

        // Create custom typeface for button text and apply to buttons
        Typeface customTypeface = Typeface.createFromAsset(getAssets(), "fonts/LeagueSpartan-Bold.otf");
        Button playButton = (Button) findViewById(R.id.buttonPlay);
        Button aboutButton = (Button) findViewById(R.id.buttonAbout);
        Button exitButton = (Button) findViewById(R.id.buttonExit);
        if (playButton != null) {
            playButton.setTypeface(customTypeface);
        }
        if (aboutButton != null) {
            aboutButton.setTypeface(customTypeface);
        }
        if (exitButton != null) {
            exitButton.setTypeface(customTypeface);
        }

        // Associate image views
        ImageView imageViewMenuMuch = (ImageView) findViewById(R.id.imageViewMenuMuch);
        ImageView imageViewMenuW = (ImageView) findViewById(R.id.imageViewMenuW);
        ImageView imageViewMenuO = (ImageView) findViewById(R.id.imageViewMenuO);
        ImageView imageViewMenuR = (ImageView) findViewById(R.id.imageViewMenuR);
        ImageView imageViewMenuD = (ImageView) findViewById(R.id.imageViewMenuD);

        /* This code to delay animation was adapted from an answer to a post by user Xaver Kapeller on Stack Overflow.
         * References: Animation loop: Animate Views one after another
         *             http://stackoverflow.com/questions/24889019/animate-views-one-after-another
         */
        final ImageView[] animationImageViews = new ImageView[]{imageViewMenuMuch, imageViewMenuW, imageViewMenuO, imageViewMenuR, imageViewMenuD};

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                int animationDelay = 500;
                for (int i = 0; i < animationImageViews.length; i++) {
                    int delay = i * animationDelay;
                    final ImageView imageView = animationImageViews[i];
                    imageView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Animation menuPopAnimation = AnimationUtils.loadAnimation(MenuActivity.this, R.anim.menu_bounce);
                            imageView.setVisibility(View.VISIBLE);
                            imageView.startAnimation(menuPopAnimation);
                        }
                    }, delay);
                }
            }
        }, 250);

        /* End Xaver Kapeller's code */
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
    }

    /* References: Requesting Permissions at Run Time on Android Developers
     *             http://developer.android.com/training/permissions/requesting.html
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case NETWORK_STATE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    DBUpdater.execute();
                }
                break;
            }
            case INTERNET: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    DBUpdater.execute();
                }
                break;
            }
        }
    }

    // Hides the action bar
    private void hideActionBar() {
        if (getActionBar() != null) {
            getActionBar().hide();
        }
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
    }

    /* Checks if the app is on it's first run, or if it has been updated
     * This code has been adapted from an answer to a post by user Squonk on Stack Overflow
     * References: Check if application is on its first run
     *             http://stackoverflow.com/questions/7217578/check-if-application-is-on-its-first-run
     */
    private void checkFirstRun() {
        // Get the current app version from  the package manager
        float currentAppVersion;
        try {
            currentAppVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            return;
        }

        // Get the app version, db version and number of levels saved in shared prefs
        SharedPreferences sharedPreferences = MuchWord.getContext().getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE);
        float savedAppVersion = sharedPreferences.getFloat(Constants.APP_VERSION, Constants.NOT_SET);
        float savedDBVersion = sharedPreferences.getFloat(Constants.DB_VERSION, Constants.NOT_SET);
        int numberOfLevels = sharedPreferences.getInt(Constants.NUMBER_OF_LEVELS, Constants.NOT_SET);

        // If the versions are not set, this means its the app's first run, so save the default versions
        if (savedAppVersion == Constants.NOT_SET) {
            sharedPreferences.edit().putFloat(Constants.APP_VERSION, currentAppVersion).apply();
            sharedPreferences.edit().putInt(Constants.CURRENT_LEVEL, Constants.DEFAULT_LEVEL).apply();
            sharedPreferences.edit().putInt(Constants.CURRENT_SCORE, Constants.DEFAULT_SCORE).apply();
        }
        if (savedDBVersion == Constants.NOT_SET) {
            sharedPreferences.edit().putFloat(Constants.DB_VERSION, Constants.LOADED_DB_VERSION).apply();
            // Since the database doesn't exist in the internal storage location, copy it from package directory
            try {
                if (!DBHelper.checkDatabaseExists()) {
                    DBHelper.copyDatabase();
                }
            } catch (IOException e) {
                e.printStackTrace();
                Log.d(Constants.ERROR, "Couldn't copy database " + e.getMessage());
            }
        }

        // If the saved db version is smaller than the internal db version, update the db
        if (savedDBVersion != Constants.NOT_SET && savedDBVersion < Constants.LOADED_DB_VERSION) {
            sharedPreferences.edit().putFloat(Constants.DB_VERSION, Constants.LOADED_DB_VERSION).apply();
            try {
                DBHelper.copyDatabase();

            } catch (IOException e) {
                e.printStackTrace();
                Log.d(Constants.ERROR, "Couldn't update database " + e.getMessage());
            }
        }
        if (numberOfLevels == Constants.NOT_SET) {
            DBHelper.updateNumberOfLevels();
        }

        // If the current app version is higher than the saved app version, the app has been updated
        if (currentAppVersion > savedAppVersion) {
            sharedPreferences.edit().putFloat(Constants.APP_VERSION, currentAppVersion).apply();
            sharedPreferences.edit().putFloat(Constants.DB_VERSION, Constants.LOADED_DB_VERSION).apply();
        }


    }

    /* This function checks if necessary permissions have been granted to the app
     * References: Declaring Permissions on Android Developers
     *             http://developer.android.com/training/permissions/declaring.html
     */
    private void checkPermissions() {
        int networkStatePermissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE);
        int internetPermissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET);

        // Check if network state, internet, read and write permissions have been granted and request for them if not granted
        if (networkStatePermissionCheck == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_NETWORK_STATE}, NETWORK_STATE);
        } else if (internetPermissionCheck == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, INTERNET);
        } else {
            DBUpdater.execute();
        }
    }

    // Start the game activity when the play button is clicked
    public void onButtonPlayClick(View view) {
        Intent gameActivityIntent = new Intent(this, GameActivity.class);
        startActivity(gameActivityIntent);
    }

    // Start the about activity when the about button is clicked
    public void onButtonAboutClick(View view) {
        Intent aboutActivityIntent = new Intent(this, AboutActivity.class);
        startActivity(aboutActivityIntent);}

    // Clean up and exit the app when the exit button is clicked
    public void onButtonExitClick(View view) {
        // Stop music if it is playing
        if (isMusicPlaying()) {
            Log.d(Constants.ALERT, "Sound off");
            Intent music = new Intent(MenuActivity.this, Music.class);
            stopService(music);
        }
        finish();
        System.exit(0);
    }

    /* Checks if music is already playing
    * The code to check if a service is already running was adapted to an answer to a post by user geekQ on Stack Overflow
    * References: How to check if a service is running on Android?
    *             http://stackoverflow.com/questions/600207/how-to-check-if-a-service-is-running-on-android
    */
    private boolean isMusicPlaying() {
        boolean isPlaying = false;
        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo serviceInfo : activityManager.getRunningServices(Integer.MAX_VALUE)) {
            if (Music.class.getName().equals(serviceInfo.service.getClassName())) {
                isPlaying = true;
            }
        }
        return isPlaying;
    }
}
