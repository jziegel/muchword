package j_tan.muchword;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /* This code to create a splash screen was written by following a tutorial by Vamsi Tallapudi on Code Refer
         * Reference: Title: Android Splash Screen Tutorial Using New Android Studio
         *            http://www.coderefer.com/android-splash-screen-example-tutorial
         */
        View splashDecorView = getWindow().getDecorView();
        int uiFullscreen = View.SYSTEM_UI_FLAG_FULLSCREEN;
        splashDecorView.setSystemUiVisibility(uiFullscreen);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        setContentView(R.layout.activity_splash);

        // Start the menu activity after 3 seconds
        Thread splashThread = new Thread() {
            @Override
            public void run() {
                try {
                    sleep(3000);
                    Intent menuIntent = new Intent(getApplicationContext(), MenuActivity.class);
                    startActivity(menuIntent);
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        splashThread.start();
    }
}
