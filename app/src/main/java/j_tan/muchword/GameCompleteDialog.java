package j_tan.muchword;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;

/**
 * Created by Ziggy on 16/5/2016.
 */

/* A dialog fragment to notify the user that the game is complete. It will be shown if there are
 * no more levels to play on the db.
 * References: DialogFragment on Android Developers
 *             https://developer.android.com/reference/android/app/DialogFragment.html
 */
public class GameCompleteDialog extends DialogFragment {

    public static String TAG = "Game Complete Dialog";

    static GameCompleteDialog gameComplete() {
        return new GameCompleteDialog();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        /* Animation for this class was created by adapting code from a an answer to a post by user Xavi Gil on Stack Overflow.
         * References: Show DialogFragment with animation growing from a point
         *             http://stackoverflow.com/questions/13402782/show-dialogfragment-with-animation-growing-from-a-point
         */
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        getDialog().getWindow().setBackgroundDrawableResource(R.color.transparent);
    }

    // If this activity is dismissed, the game activity should be finished and cleaned up as there are no more levels
    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        ((GameActivity) getActivity()).finishActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Set the layout and title
        View view = inflater.inflate(R.layout.game_complete_dialog_fragment, container, false);

        // Create custom typeface for button text and apply to buttons
        Typeface customTypeface = Typeface.createFromAsset(MuchWord.getContext().getAssets(), "fonts/LeagueSpartan-Bold.otf");
        Button buttonGameComplete = (Button) view.findViewById(R.id.buttonGameComplete);
        buttonGameComplete.setTypeface(customTypeface);

        // Dismiss the dialog on click
        buttonGameComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });
        return view;
    }
}
