package j_tan.muchword;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set custom actionbar
        setContentView(R.layout.activity_about);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setCustomView(R.layout.actionbar_about);
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_CUSTOM);
        }
        TextView textViewLicense = (TextView) findViewById(R.id.textViewLicense);
        if (textViewLicense != null) {
            textViewLicense.setMovementMethod(new ScrollingMovementMethod());
            textViewLicense.setText(Html.fromHtml(getString(R.string.about_license_text)));
        }
    }

    // Finish the activity if back button is clicked
    public void onAboutBackButtonClick(View view) {
        finish();
    }

    /* Send an a email using the defaul email client.
     * This code was adapted from answers to a post on Stack Overflo by user hrehman
     * References: How to open Email program via Intents (but only an Email program)
     *             http://stackoverflow.com/questions/3312438/how-to-open-email-program-via-intents-but-only-an-email-program
     */
    public void onEmailButtonClick(View view) {
        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        emailIntent.setType("vnd.android.cursor.item/email");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[] {"jtandev305@gmail.com"});
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Feedback on MuchWord App");
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Hi JTan,");
        startActivity(Intent.createChooser(emailIntent, "Send mail using..."));
    }
}
