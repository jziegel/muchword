package j_tan.muchword;

import android.graphics.Bitmap;

/* This class stores data about words retrieved from the database. The class stores the word, and the
 * two bitmap images associated with the word.
 */
public class Word {

    // Private properties
    private String word;
    private Bitmap image1;
    private Bitmap image2;

    // Methods to get and set properties
    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public Bitmap getImage1() {
        return image1;
    }

    public void setImage1(Bitmap image1) {
        this.image1 = image1;
    }

    public Bitmap getImage2() {
        return image2;
    }

    public void setImage2(Bitmap image2) {
        this.image2 = image2;
    }
}
